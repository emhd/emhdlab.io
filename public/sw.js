var cacheName = 'emhd';
var filesToCache = [
	'/',
    '/index.html',
    '/script.js',
];

self.addEventListener('install', e => {
	e.waitUntil( (async () => {
		await caches.delete(cacheName);
		caches.open(cacheName).then( cache => {
			return cache.addAll(filesToCache);
		});
	})());
});


self.addEventListener('fetch', e => {
    e.respondWith(
        caches.match(e.request).then(response => {
            return response || fetch(e.request);
        })
    );
});
