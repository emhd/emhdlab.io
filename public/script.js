window.onload = () => {
	'use strict';
	let parent = document.getElementById("buttons");
	let select = document.querySelector("select");
	let my = JSON.parse(localStorage.getItem("my"));
	if (!my) {
		my = new Map([
			[325, "Zoo"],
			[18, "Botanická"],
			[233, "Nový most"],
			[170, "Lipského"],
			[86, "Horné Krčatce"],
			[82, "Hl. stanica"],
			[4, "Autobuska"],
			[274, "SAV"],
			[448, "Zochová"],
		]);
	} else {
		my = new Map(my);
	}

	let show;
	let portal = document.querySelector("iframe");
	let default_src = "https://imhd.sk/ba/online-zastavkova-tabula?theme=black&I_CONSENT=TO_THE_COOKIES_POLICY_AND_THE_PRIVACY_POLICY&showInfoText=0&nthDisplay=";
	let buttons = new Map();

	function update(id) {
		let button1 = buttons.get(show);
		let button2 = buttons.get(id);
		if (button1) button1.classList.toggle("active");
		else select.classList.toggle("active");

		if (button2) {
			button2.classList.toggle("active");
			button2.scrollIntoView();
		} else select.classList.toggle("active");

		show = id;
		if (show > 0) {
			portal.src = default_src+"1&st="+show;
		} else {
			portal.src = default_src+(-show);
		}
	}

	document.getElementById("next").addEventListener("click", () => {
		let active = parent.getElementsByClassName("active");
		if (active[0]) {
			let next = active[0].nextElementSibling;
			if (next) {
				next.click();
			} else {
				parent.firstElementChild.click();
			}
		} else {
			parent.firstElementChild.click();
		}
	});

	document.getElementById("prev").addEventListener("click", () => {
		let active = parent.getElementsByClassName("active");
		if (active[0]) {
			let previous = active[0].previousElementSibling;
			if (previous) {
				previous.click();
			} else {
				parent.lastElementChild.click();
			}
		} else {
			parent.lastElementChild.click();
		}
	});

	function add_button(name, value, par){
		let e = document.createElement ("Button");
		e.addEventListener("click", e => update(value));
		e.innerHTML = name;
		par.appendChild(e);
		buttons.set(value, e);
		update(value);
	}

		document.getElementById("plus").addEventListener("click", () => {
		let value = parseInt(select.options[select.selectedIndex].value);
		if (!buttons.has(value)) {
			add_button(select.options[select.selectedIndex].innerHTML, value, parent);
			my.set(value, select.options[select.selectedIndex].innerHTML);
			localStorage.setItem("my", JSON.stringify(Array.from(my.entries())));
		} else {
			alert("Táto zastávka už je uložená medzi oblúbenými!")
		}
	});

	document.getElementById("minus").addEventListener("click", () => {
		let value = parseInt(select.options[select.selectedIndex].value);
		if (!buttons.has(value)) {
			alert("Táto zastávka nie je uložená medzi oblúbenými!")
		} else {
			buttons.get(value).remove();
			buttons.delete(value);
			my.delete(value, select.options[select.selectedIndex].innerHTML);
			localStorage.setItem("my", JSON.stringify(Array.from(my.entries())));
		}
	});

	select.addEventListener("change", e => {
		let value = parseInt(select.options[select.selectedIndex].value);
		update(value);
	});

	for (let i = 1; i <= 4; i++) add_button(i+".", -i, document.getElementById("close"));
	my.forEach( (name, value) => add_button(name, value, parent));
	update(-1);
	parent.firstElementChild.scrollIntoView();

	if ('serviceWorker' in navigator) {
		navigator.serviceWorker.register('./sw.js')
			.then(function() { console.log('Service Worker Registered'); });
	}
}
